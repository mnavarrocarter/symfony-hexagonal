<?php

namespace App\Infrastructure\Messaging\Symfony;

use Symfony\Component\Messenger\MessageBus;

/**
 * This is your application bus.
 *
 * @package App\Infrastructure\Messaging\Symfony
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
class AppBus extends MessageBus
{
}