<?php

namespace App\Exception;

/**
 * The base exception class for your application.
 * @package App\Exception
 */
class AppException extends \DomainException
{

}