The Domain Layer
================

The purpose of the domain layer is to hold all the business logic of your
application. Here you define your models, your aggregates (classes that conform 
your models, like different value objects), your repository interfaces, and the events
of your application.