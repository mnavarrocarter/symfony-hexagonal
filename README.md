Symfony Hexagonal Architecture Skeleton
=======================================

This skeleton comes preconfigured to work with hexagonal architecture. It has everything
separated and ready to start working with it.

Just run:

    composer create-project mnavarrocarter/hexagonal-symfony <folder>
    
 